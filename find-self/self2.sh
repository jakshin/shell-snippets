#!/bin/dash
#!/bin/sh
# This is like self.sh, but works on dash and BSD's fork of Almquist sh
set -e

# echo '$0:' "$0"
self="$0"
case "$self" in
	/*) ;; # Already absolute
	*) self="$(pwd)/$self";;
esac

while [ -L "$self" ]; do
	if ! target="$(readlink -- "$self")"; then
		# We've resolved all symlinks
		break
	fi

	case "$target" in
		/*) self="$target";;
		*) self="$(dirname -- "$self")/$target";; # Relative symlink
	esac
done

self_dir="$(cd -- "$(dirname -- "$self")" && pwd)"
self="$self_dir/$(basename -- "$self")"

echo "self: $self"
