#!/usr/bin/env bash
# Tests for self.sh (and self2.sh)

# Tested on:
# + bash 3.2 on macOS, zsh 5.8 on macOS
# + bash 4.4 on CentOS
# + bash 5 on Fedora
# + bash 5 on Solus
# + bash 5 on Ubuntu (doesn't work in dash, see self2.sh)
# + bash 5 on GhostBSD (doesn't work in BSD's sh, see self2.sh)

script_dir="$(dirname -- "$0")"
cd -- "$script_dir"
script_dir="$(pwd)"

self_sh="self.sh"  # Or self2.sh, for dash and BSD's sh

shebang="$(head -n 1 $self_sh)"
shell="${shebang//\#\!/}" 
echo -e "Testing $self_sh with $shell\n"

trap cleanup EXIT
function cleanup() {
	cd -- "$script_dir"
	rm -rf -- --dir1 --dir2
}

"$PWD/$self_sh"
$shell "$PWD/$self_sh"

./$self_sh
$shell $self_sh

mkdir -p -- --dir1 && (
	cd -- --dir1

	ln -s "$script_dir/$self_sh" "link-to-$self_sh"
	ln -s "link-to-$self_sh" "link-to-link-to-$self_sh"  # Relative link
	ls -l *.sh

	"./link-to-$self_sh"
	$shell "link-to-$self_sh"

	"./link-to-link-to-$self_sh"
	$shell "link-to-link-to-$self_sh"
)

mkdir -p -- --dir2 && (
	cd -- --dir2
	"../--dir1/link-to-link-to-$self_sh"
	$shell "../--dir1/link-to-link-to-$self_sh"
)

PATH="$PATH:$PWD/--dir1"
"link-to-link-to-$self_sh"

PATH="$PATH:."
$self_sh

echo
echo "Tests completed"
