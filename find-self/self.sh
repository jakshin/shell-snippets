#!/usr/bin/env bash
#!/usr/bin/env zsh
set -e

# echo '$0:' "$0"
self="$0"
[[ $self == /* ]] || self="$(pwd)/$self"

while [[ -L $self ]]; do
	if ! target="$(readlink -- "$self")"; then
		# We've resolved all symlinks
		break
	elif [[ $target != /* ]]; then
		# Relative symlink
		self="$(dirname -- "$self")/$target"
	else
		self="$target"
	fi
done

self_dir="$(cd -- "$(dirname -- "$self")" && pwd)"
self="$self_dir/$(basename -- "$self")"

echo "self: $self"
