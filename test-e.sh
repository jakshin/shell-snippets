#!/bin/bash

set -e

echo "Checking..."

quiet=false
opts=(a b)
[[ $quiet == true ]] && opts+=(-q)
[[ $quiet == true ]]  # bash 5 stops here, macOS's bash 3.2 doesn't

echo "Check complete"
echo "${opts[@]}"
