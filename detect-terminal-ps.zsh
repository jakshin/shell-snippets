#!/usr/bin/env zsh
# POC of detecting what terminal emulator we're running under using the process tree.
# There are many circumstances in which this approach doesn't and can't ever work,
# so it's not suitable as the sole or even primary means of detecting the terminal,
# but it might be useful sometimes, e.g. to distinguish between VTE-based terminals.

term_cmd="???"

if [[ -n $SSH_TTY ]]; then
	# This kind of terminal check doesn't work in any remote shell (ssh, telnet, rlogin, etc),
	# but we only try to detect SSH; we could also check $SSH_CLIENT and $SSH_CONNECTION
	print -u2 "This doesn't work in SSH (or any remote shell)"

elif [[ $OS == Windows* || -n $WSL_DISTRO_NAME ]]; then
	print -u2 "This doesn't work on Windows (WSL, Cygwin or MSYS2)"

elif [[ $OSTYPE == darwin* ]]; then
	# On macOS, walk up parent processes until finding the one whose parent is launchd;
	# on Ventura, this works in Apple Terminal, iTerm, Tabby, WezTerm, VS Code, IntelliJ & other JetBrains IDEs,
	# including in sudo; on Big Sur, it works in at least Terminal, iTerm, and VS Code (I didn't test others)
	pid=$$

	while true; do
		ps_info=($(ps -o ppid=,comm= $pid))
		pid=$ps_info[1]
		term_cmd=${ps_info[*]:1}  # Second argument onward (so we handle spaces in paths correctly)

		if [[ $ps_info[1] == 1 ]]; then
			break
		fi
	done

elif [[ $OSTYPE == linux* || $OSTYPE == *bsd* ]]; then
	if [[ $OSTYPE == linux* ]] && inode=$(stat -c %i / 2> /dev/null) && (( inode > 256 )); then
		# Probably also doesn't work in BSD jails, but we don't try to detect that here
		print -u2 "This doesn't work when docker running or exec-ing into a container"
	else
		# This works in every distro, desktop environment, and terminal emulator I've tried,
		# even VS Code and IntelliJ, and under sudo, and su (su only tested on BSD & some Linuxes)
		pid=$$
	
		while true; do
			# The session leader's parent process's ID and full command line
			# Based on https://stackoverflow.com/a/63721547
			ps_info=($(ps -o sid= -p $pid | xargs ps -o ppid= -p | xargs ps -o pid= -o args= -p))
			term_pid=$ps_info[1]
			term_cmd=${ps_info[*]:1}
			term_cmd_base="$(basename "$ps_info[2]")"
	
			if [[ $term_cmd_base == sudo ]]; then
				# Start over, as though sudo were the current process
				pid=$term_pid
			elif [[ $term_cmd_base == init ]]; then  # e.g. "/sbin/init splash" on Ubuntu & Pop_OS
				term_cmd="[console]"
				break
			else
				# The terminal command could be a symlink (e.g. in Kali's "Root Terminal Emulator",
				# where /usr/bin/x-terminal-emulator is a link to a link to /usr/bin/qterminal),
				# which we don't bother to resolve
				break
			fi
		done
	fi

elif [[ $OSTYPE == haiku ]]; then
	print -u2 "Not implemented on Haiku yet"
else
	print -u2 "Unrecognized OS/environment $OSTYPE"
fi

echo "Terminal: $term_cmd"
[[ $term_cmd != "???" ]]
