#!/bin/bash
# Tested and working on macOS's bash 3.2 and Ubuntu's bash 5.1.8

values="one two three"
others="lah dee dooby"

# string -> array
for var in values others; do
	eval "$var=(\${$var[@]})"
	declare -p $var
done

# already arrays
for var in values others; do
	eval "$var=(\${$var[@]})"
	declare -p $var
done
